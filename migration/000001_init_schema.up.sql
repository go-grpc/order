CREATE TABLE IF NOT EXISTS orders (
    id serial PRIMARY KEY not null ,
    user_id int not null,
    total_price int not null default 0,
    status varchar(25) not null default 'created'
);

SELECT * FROM orders WHERE id = 2