package app

import (
	grpc2 "bus/order/internal/adapter/grpc"
	"bus/order/internal/adapter/kafka"
	"bus/order/internal/domain/service"
	"bus/order/internal/domain/storage"
	"bus/order/pkg/client/postgresql"
	"bus/order/pkg/httpserver"
	kafkaClient "bus/order/pkg/kafka"
	"bus/order/pkg/logger"
	"context"
	"fmt"
	"github.com/joho/godotenv"
	kafka2 "github.com/segmentio/kafka-go"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"net"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

type App struct {
	repo           storage.OrderStorage
	service        service.OrderService
	postgresql     postgresql.Client
	logger         *logger.Logger
	ctx            context.Context
	grpcServer     *grpc.Server
	httpserver     *httpserver.Server
	kafkaClient    *kafkaClient.ConsumerGroups
	orderProcessor *kafka.OrderMessageProcessor
	kafkaConn      *kafka2.Conn
	pullSize       int
}

func New() (*App, error) {
	err := godotenv.Load()
	if err != nil {
		return nil, err
	}
	a := &App{}
	k, err := kafka2.DialContext(context.Background(), "tcp", "localhost:9092")
	if err != nil {
		a.logger.Fatal(err)
	}
	pullSize, err := strconv.Atoi(os.Getenv("PULL_SIZE"))
	a.pullSize = pullSize
	a.kafkaConn = k
	a.httpserver = httpserver.New(nil, httpserver.Port(os.Getenv("PORT")))
	a.ctx = context.Background()
	a.logger = logger.New(os.Getenv("LOG_LEVEL"))
	client, err := postgresql.New(a.ctx,
		5,
		os.Getenv("POSTGRES_USER"),
		os.Getenv("postgres_password"),
		os.Getenv("postgres_host"),
		os.Getenv("POSTGRES_PORT"),
		os.Getenv("POSTGRES_DB"))
	a.postgresql = client
	a.repo = storage.New(a.postgresql, a.logger)
	a.service = service.New(a.logger, a.repo)
	a.kafkaClient = kafkaClient.NewConsumerGroup([]string{}, "1", a.logger)
	fmt.Println(a.service, "service")
	a.orderProcessor = kafka.New(a.logger, a.service)
	return a, nil

}

func (a App) Run() {

	defer func(kafkaConn *kafka2.Conn) {
		err := kafkaConn.Close()
		if err != nil {
			panic(err)
		}
	}(a.kafkaConn)
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)
	listener, err := net.Listen("tcp", ":5000")
	if err != nil {

	}
	srv := grpc.NewServer()
	r := kafka2.NewReader(kafka2.ReaderConfig{
		Brokers:     []string{"localhost:9092"},
		GroupID:     "my-group",
		GroupTopics: []string{"UPDATE_ORDER", "CREATE_ORDER"},
	})
	g, _ := errgroup.WithContext(a.ctx)
	go a.orderProcessor.ProcessMessages(a.ctx, r, 1)

	grpc2.Run(srv, a.service, a.logger)
	g.Go(func() error {
		return srv.Serve(listener)
	})
	err = g.Wait()
	if err != nil {
		return
	}

	select {
	case s := <-interrupt:
		a.logger.Info("app - Run - signal: " + s.String())
	case err := <-a.httpserver.Notify():
		a.logger.Error(fmt.Errorf("app - Run - httpServer.Notify: %w", err))
	}

	// Shutdown

}
