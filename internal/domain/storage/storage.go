package storage

import (
	"bus/order/internal/domain/model"
	"context"
)

//go:generate mockery --dir . --name OrderStorage --output order/mocks
type OrderStorage interface {
	Save(ctx context.Context, dto model.CreateOrderDto) error
	Find(ctx context.Context, userId uint32) ([]model.Order, error)
	FindOneById(ctx context.Context, orderId uint32) (model.Order, error)
	UpdateStatus(ctx context.Context, dto model.UpdateOrderStatusDto) error
}
