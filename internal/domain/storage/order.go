package storage

import (
	"bus/order/internal/domain/model"
	"bus/order/pkg/client/postgresql"
	"bus/order/pkg/exception"
	"bus/order/pkg/logger"
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
)

type Storage struct {
	r      postgresql.Client
	logger *logger.Logger
}

func (s Storage) Save(ctx context.Context, dto model.CreateOrderDto) error {
	s.logger.Debug(fmt.Sprintf("creating order to user: %d", dto.UserId))
	q := `INSERT INTO orders (user_id) VALUES($1)`
	rows, err := s.r.Exec(ctx, q, dto.UserId)
	if err != nil {
		return fmt.Errorf("заказ: %w", exception.ErrorFromDB())
	}
	if !rows.Insert() {
		return fmt.Errorf("заказ: %w", exception.ErrorOnCreating())
	}
	s.logger.Debug(fmt.Sprintf("created order to user: %d", dto.UserId))
	return nil
}

func (s Storage) Find(ctx context.Context, userId uint32) ([]model.Order, error) {
	s.logger.Debug(fmt.Sprintf("geting orders to user: %d", userId))
	var result []model.Order
	q := `SELECT *  FROM orders WHERE user_id = $1`
	rows, err := s.r.Query(ctx, q, userId)
	if err != nil {
		return result, fmt.Errorf("заказ: %w", exception.ErrorFromDB())
	}
	var order model.Order
	for rows.Next() {
		if err = rows.Scan(&order.Id, &order.UserId, &order.TotalPrice, &order.Status); err != nil {
			return result, fmt.Errorf("заказ: %w", exception.DecodeException())
		}
		result = append(result, order)
	}
	s.logger.Debug(fmt.Sprintf("got orders to user: %d", userId))
	return result, nil
}

func (s Storage) FindOneById(ctx context.Context, orderId uint32) (model.Order, error) {
	s.logger.Debug(fmt.Sprintf("geting one order by id: %d", orderId))
	var result model.Order
	q := `SELECT id,user_id,status,total_price FROM orders WHERE id = $1`
	rows, err := s.r.Query(ctx, q, orderId)
	if err != nil {
		if err == pgx.ErrNoRows {
			return model.Order{}, fmt.Errorf("заказ: %d %w ", orderId, exception.NotFoundException())
		}
		return result, fmt.Errorf("заказ: %w", exception.ErrorFromDB())
	}
	for rows.Next() {
		if err = rows.Scan(&result.Id, &result.UserId, &result.Status, &result.TotalPrice); err != nil {
			return result, fmt.Errorf("заказ: %w", exception.DecodeException())
		}
	}
	s.logger.Debug(fmt.Sprintf("got one order by id: %d", orderId))
	return result, nil
}

func (s Storage) UpdateStatus(ctx context.Context, dto model.UpdateOrderStatusDto) error {
	s.logger.Debug(fmt.Sprintf("updating  order by id: %d", dto.OrderId))
	q := `UPDATE orders SET status = $1 WHERE id = $2`
	rows, err := s.r.Exec(ctx, q, dto.Status, dto.OrderId)
	if err != nil {
		return fmt.Errorf("заказ: %w", exception.ErrorFromDB())
	}
	if !rows.Update() {
		return fmt.Errorf("заказ: %d %w", dto.OrderId, exception.ErrorOnUpdating())
	}
	s.logger.Debug(fmt.Sprintf("updated  order by id: %d", dto.OrderId))
	return nil
}

func New(r postgresql.Client, logger *logger.Logger) OrderStorage {
	return Storage{r, logger}
}
