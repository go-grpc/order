package model

type Order struct {
	Id         uint32 `json:"id"`
	UserId     uint32 `json:"user_id"`
	TotalPrice uint32 `json:"total_price"`
	Status     string `json:"status"`
}
type OrderOne struct {
	Order
	CartItems []CartItem `json:"cart_items"`
}
type CartItem struct {
	Id        uint32 `json:"id"`
	ProductId string `json:"product_id"`
	Qty       uint32 `json:"qty"`
}
type DeleteOrder struct {
	OrderId uint32 `json:"order_id"`
}

type CreateOrderDto struct {
	UserId uint32 `json:"user_id"`
}

type UpdateOrderStatusDto struct {
	OrderId uint32 `json:"order_id"`
	Status  string `json:"status"`
}
