package service

import (
	"bus/order/internal/domain/model"
	"bus/order/internal/domain/storage"
	"bus/order/pkg/logger"
	"context"
)

type order struct {
	logger  *logger.Logger
	storage storage.OrderStorage
}

func (o order) Create(ctx context.Context, dto model.CreateOrderDto) error {
	o.logger.Debug("sending query to db for create order")
	err := o.storage.Save(ctx, dto)
	if err != nil {
		return err
	}
	o.logger.Debug("success query to db for create order")
	return nil
}

func (o order) Update(ctx context.Context, dto model.UpdateOrderStatusDto) error {
	o.logger.Debug("sending query to db for order update")
	err := o.storage.UpdateStatus(ctx, dto)
	if err != nil {
		return err
	}
	o.logger.Debug("success query to db for order update")
	return nil
}

func (o order) GetAll(ctx context.Context, userId uint32) ([]model.Order, error) {
	o.logger.Debug("sending query to db for get orders")
	res, err := o.storage.Find(ctx, userId)
	if err != nil {
		return res, err
	}
	o.logger.Debug("success query to db for get orders")
	return res, nil
}

func (o order) GetOne(ctx context.Context, orderId uint32) (model.OrderOne, error) {
	o.logger.Debug("sending query to db for get one order")
	res, err := o.storage.FindOneById(ctx, orderId)
	if err != nil {
		return model.OrderOne{}, err
	}
	o.logger.Debug("success query to db for get one order")
	return model.OrderOne{Order: res}, err
}

func New(logger *logger.Logger, storage storage.OrderStorage) OrderService {
	return order{
		logger:  logger,
		storage: storage,
	}
}
