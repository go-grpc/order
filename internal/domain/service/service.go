package service

import (
	"bus/order/internal/domain/model"
	"context"
)

type OrderService interface {
	Create(ctx context.Context, dto model.CreateOrderDto) error
	Update(ctx context.Context, dto model.UpdateOrderStatusDto) error
	GetAll(ctx context.Context, userId uint32) ([]model.Order, error)
	GetOne(ctx context.Context, orderId uint32) (model.OrderOne, error)
}
