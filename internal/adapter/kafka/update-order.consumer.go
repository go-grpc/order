package kafka

import (
	"bus/order/internal/domain/model"
	"context"
	"encoding/json"
	"fmt"
	"github.com/segmentio/kafka-go"
	"golang.org/x/sync/errgroup"
)

func (o OrderMessageProcessor) processUpdateOrder(ctx context.Context, r *kafka.Reader, m kafka.Message) {
	o.logger.Debug("start process updating order")

	var msg model.UpdateOrderStatusDto
	if err := json.Unmarshal(m.Value, &msg); err != nil {
		o.logger.Debug("start process updating order")
		return
	}
	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		return o.service.Update(ctx, msg)
	})
	err := g.Wait()
	if err != nil {
		o.logger.Debug(fmt.Sprintf("error on update order processing: %s", err.Error()))
		return
	}
	o.logger.Debug("success end update order process")

}
