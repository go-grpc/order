package kafka

import (
	"bus/order/internal/domain/model"
	"context"
	"encoding/json"
	"fmt"
	"github.com/segmentio/kafka-go"
	"golang.org/x/sync/errgroup"
)

func (o OrderMessageProcessor) createOrderProcess(ctx context.Context, r *kafka.Reader, m kafka.Message) {
	o.logger.Debug("start process creating order")
	defer o.logger.Debug("end process creating order")
	var msg model.CreateOrderDto
	if err := json.Unmarshal(m.Value, &msg); err != nil {
		o.logger.Error("error in unmarshal")
		return
	}

	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		fmt.Println(o.service)
		return o.service.Create(ctx, msg)
	})
	err := g.Wait()
	if err != nil {
		o.logger.Error("error in creating order")
		return
	}
	o.logger.Debug("success process creating order")

}
