package kafka

import (
	"bus/order/internal/domain/model"
	"bus/order/pkg/logger"
	"context"
	"fmt"
	"github.com/segmentio/kafka-go"
	"os"
)

type service interface {
	Create(ctx context.Context, dto model.CreateOrderDto) error
	Update(ctx context.Context, dto model.UpdateOrderStatusDto) error
}
type OrderMessageProcessor struct {
	logger  *logger.Logger
	service service
}

func New(logger *logger.Logger, service service) *OrderMessageProcessor {
	return &OrderMessageProcessor{logger: logger, service: service}
}
func (o OrderMessageProcessor) ProcessMessages(ctx context.Context, r *kafka.Reader, workerID int) {
	fmt.Println(o.service, "service")

	for {
		select {
		case <-ctx.Done():
			return
		default:

		}

		m, err := r.FetchMessage(ctx)
		fmt.Println(err)
		if err != nil {
			o.logger.Error(err, workerID)
			continue
		}
		switch m.Topic {
		case os.Getenv("ORDER_CRETE_EVENT"):
			o.createOrderProcess(ctx, r, m)
		case os.Getenv("ORDER_UPDATE_EVENT"):
			o.processUpdateOrder(ctx, r, m)
		}

	}
}
