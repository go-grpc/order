package grpc

import (
	"bus/order/internal/adapter/grpc/proto"
	"bus/order/internal/domain/model"
	"bus/order/pkg/exception"
	"bus/order/pkg/logger"
	"context"
	"errors"
	"fmt"
	"google.golang.org/grpc"
)

type service interface {
	GetAll(ctx context.Context, userId uint32) ([]model.Order, error)
	GetOne(ctx context.Context, orderId uint32) (model.OrderOne, error)
}
type server struct {
	s service
	l *logger.Logger
}

func Run(gServer *grpc.Server, s service, l *logger.Logger) {
	server := server{s, l}
	proto.RegisterOrderServiceServer(gServer, server)
}

func (s server) GetOrders(ctx context.Context, request *proto.GetOrdersRequest) (*proto.GetOrdersResponse, error) {
	s.l.Debug("request to get orders")
	res, err := s.s.GetAll(ctx, request.UserId)
	defer s.l.Debug("get orders request end")
	var grpcRes []*proto.Order
	r := &proto.Response{
		Status: 200,
		Error:  "",
	}
	if err != nil {
		if errors.Is(err, exception.ErrorFromDB()) {
			s.l.Debug("error request from db")
			r.Error = "ошибка в базе данных"
			r.Status = 500
			return &proto.GetOrdersResponse{Orders: nil, Response: r}, nil
		}
		r.Error = "что то пошло не так"
		return &proto.GetOrdersResponse{Orders: nil, Response: r}, nil
	}
	for _, order := range res {
		fmt.Println(order)
		grpcRes = append(grpcRes, &proto.Order{
			Id:         order.Id,
			TotalPrice: uint64(order.TotalPrice),
			Status:     order.Status,
		})
	}
	return &proto.GetOrdersResponse{Orders: grpcRes, Response: r}, nil
}

func (s server) GetOrderOne(ctx context.Context, request *proto.GetOrderByIdRequest) (*proto.GetOrderByIdResponse, error) {
	s.l.Debug(fmt.Sprintf("request to get one order :%d", request.Id))
	res, err := s.s.GetOne(ctx, request.GetId())
	r := &proto.Response{
		Status: 200,
		Error:  "",
	}
	if err != nil {
		fmt.Println(err)
		switch err {
		case exception.NotFoundException():
			r.Error = err.Error()
			r.Status = 404
			return &proto.GetOrderByIdResponse{Response: r}, nil
		case exception.ErrorFromDB():
			r.Error = err.Error()
			r.Status = 500
			return &proto.GetOrderByIdResponse{Response: r}, nil
		case exception.DecodeException():
			r.Error = err.Error()
			r.Status = 422
			return &proto.GetOrderByIdResponse{Response: r}, nil
		default:
			r.Error = "что то пошло не так"
			r.Status = 500
			return &proto.GetOrderByIdResponse{Response: r}, nil
		}
	}
	return getOrderOne(res, r), nil

}
