package grpc

import (
	"bus/order/internal/adapter/grpc/proto"
	"bus/order/internal/domain/model"
)

func getOrderOne(one model.OrderOne, response *proto.Response) *proto.GetOrderByIdResponse {
	var cartItems []*proto.CartItem
	res := &proto.GetOrderByIdResponse{}
	res.Id = one.Id
	res.Status = one.Status
	res.Response = response
	res.TotalPrice = uint64(one.TotalPrice)
	for _, item := range one.CartItems {
		cartItems = append(cartItems, &proto.CartItem{
			Id:        item.Id,
			ProductId: item.ProductId,
			Qty:       item.Qty,
		})
	}
	return res
}
