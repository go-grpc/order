package exception

import "errors"

type ServerError struct {
	err error
}

func (s ServerError) Error() string {
	return s.err.Error()
}

func New(msg string) error {
	return &ServerError{err: errors.New(msg)}
}

var notFound = New("не найден")
var exist = New("уже существует")
var decode = New("decode")
var internalError = New("что то пошло не так")
var errorFromDb = New("ошибка база данных")
var errorOnCreating = New("ошибка при созданий")
var errorOnUpdating = New("ошибка при обновлений")

func NotFoundException() error {
	return notFound
}
func ErrorOnUpdating() error {
	return errorOnUpdating
}
func InternalError() error {
	return internalError
}
func ErrorOnCreating() error {
	return errorOnCreating
}
func ErrorFromDB() error {
	return errorFromDb
}
func ExistException() error {
	return exist
}
func DecodeException() error {
	return decode
}
