package kafka

import (
	"bus/order/pkg/logger"
	"context"
	"github.com/segmentio/kafka-go"
	"sync"
)

type Worker func(ctx context.Context, r *kafka.Reader, wg *sync.WaitGroup, workerID int)

type ConsumerGroup interface {
	ConsumeTopic(ctx context.Context, cancel context.CancelFunc, groupID, topic string, poolSize int, worker Worker)
	GetNewKafkaReader(kafkaURL []string, topic, groupID string) *kafka.Reader
}
type ConsumerGroups struct {
	Brokers []string
	GroupID string
	log     *logger.Logger
}

func NewConsumerGroup(brokers []string, groupID string, log *logger.Logger) *ConsumerGroups {
	return &ConsumerGroups{Brokers: brokers, GroupID: groupID, log: log}
}

func (c *ConsumerGroups) GetNewKafkaReader(kafkaURL []string, groupTopics []string, groupID string) *kafka.Reader {
	return kafka.NewReader(kafka.ReaderConfig{
		Brokers:                kafkaURL,
		GroupID:                groupID,
		GroupTopics:            groupTopics,
		MinBytes:               minBytes,
		MaxBytes:               maxBytes,
		QueueCapacity:          queueCapacity,
		HeartbeatInterval:      heartbeatInterval,
		CommitInterval:         commitInterval,
		PartitionWatchInterval: partitionWatchInterval,
		MaxAttempts:            maxAttempts,
		MaxWait:                maxWait,
		Dialer: &kafka.Dialer{
			Timeout: dialTimeout,
		},
	})
}

func (c *ConsumerGroups) ConsumeTopic(ctx context.Context, groupTopics []string, poolSize int, worker Worker) {
	c.log.Debug("start")
	r := c.GetNewKafkaReader(c.Brokers, groupTopics, c.GroupID)

	defer func() {
		if err := r.Close(); err != nil {
			c.log.Warn("ConsumerGroups.r.Close: %v", err)
		}
	}()

	c.log.Info("Starting consumer groupID: %s, topic: %+v, pool size: %v", c.GroupID, groupTopics, poolSize)

	wg := &sync.WaitGroup{}
	for i := 0; i <= poolSize; i++ {
		wg.Add(1)
		go worker(ctx, r, wg, i)
	}
	wg.Wait()
}
